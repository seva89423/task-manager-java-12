package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.IProjectController;
import ru.zorin.tm.api.service.IProjectService;
import ru.zorin.tm.model.Project;
import ru.zorin.tm.model.Task;
import ru.zorin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    public IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    private void showProject(Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjects(){
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearProjects(){
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if(project == null){
            System.out.println("[ERROR]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findProjectByIndex(index);
        if(project == null){
            System.out.println("[ERROR]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findProjectByName(name);
        if(project == null){
            System.out.println("[ERROR]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null){
            System.out.println("[ERROR]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null){
            System.out.println("[ERROR]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findProjectByIndex(index);
        if(project == null){
            System.out.println("[ERROR]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project taskUpdated = projectService.updateProjectByIndex(index, name, description);
        if (taskUpdated == null){
            System.out.println("[ERROR]");
            return;
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectById(id);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectByName(name);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.removeProjectByIndex(index);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[COMPLETE]");
    }
}