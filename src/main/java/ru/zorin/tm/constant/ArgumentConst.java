package ru.zorin.tm.constant;

public interface ArgumentConst {

    String ARG_HELP = "-h";

    String ARG_ABOUT = "-a";

    String ARG_VERSION = "-v";

    String ARG_INFO = "-i";
}