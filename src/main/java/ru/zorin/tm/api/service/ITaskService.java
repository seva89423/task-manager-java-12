package ru.zorin.tm.api.service;

import ru.zorin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneById(String id);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);
}
