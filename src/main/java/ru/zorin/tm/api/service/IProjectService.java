package ru.zorin.tm.api.service;

import ru.zorin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findProjectById(String id);

    Project findProjectByIndex(Integer index);

    Project findProjectByName(String name);

    Project removeProjectByName(String name);

    Project removeProjectByIndex(Integer index);

    Project removeProjectById(String id);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);
}
